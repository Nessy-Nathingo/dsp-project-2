import ballerina/http;
import ballerina/log;

//The Target Endpoint
endpoint http:Client targetEndpoint {
   url: "https://register-candidate"
};


@http:ServiceConfig {
   basePath: "/gateway/1.0.0"
}
service<http:Service> passthrough bind { port: 9090 } {

   @http:ResourceConfig {
       methods:["GET"],
       path: "/menu"
   }
   passthrough(endpoint caller, http:Request req) {
       var clientResponse = targetEndpoint->forward("/info", req);

       match clientResponse {

           http:Response res => {
               caller->respond(res) but { error e =>
                          log:printError("Error sending response", err = e) };
           }

           error err => {
               http:Response res = new;
               res.statusCode = 500;
               res.setPayload(err.message);
               caller->respond(res) but { error e =>
                          log:printError("Error sending response", err = e) };
           }
       }
   }
}