import ballerina/kafka;
import ballerina/log;
import ballerina/kubernetes;
import ballerina/http;
import ballerinax/docker;


kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9092",

    clientId: "basic-producer",
    acks: "all",
    retryCount: 3,
    valueSerializerType: kafka:SER_STRING,
	keySerializerType: kafka:SER_INT
};

kafka:Producer kafkaProducer = new (producerConfiguration);

@docker:Expose{}

endpoint graphql:Listener graphqlListener{
    port:9091;
}
@http:ServiceConfig{
    basepath: "/registerCandidate"
}
@docker:Config {
	name: "producer",
	tag: "v1.0"
}

@kubernetes:Service {
    serviceType:"NodePort";
    name: "votingsystem";
}

endpoint graphql:Listener graphqlListener{
    port:9091;
}

@kubernetes:Deployment {
    image:"producer";
    name:"votingsystem";
}
@http:ServiceConfig{
    basepath: "/registerCandidate"
}

type Candidate record {
    string name;
    int id;
    string position;
    string measure; 

};
listener http:Listener httpListener = new(9091);

@http:ServiceConfig{
    basepath: "/register-candidate"
}

service VoterManagement on httpListener {

     resource function info(http:Caller caller, http:Request request) {
        
        Candidate candidate1 = {
            name: "Thomas Muller",
            id: 20201,
            position: "Head of Department",
            measure: "No open shoes in cafeteria"
        };

      var sendRes = prod -> send(candidate1.toString(), "registered-candidates", partition = 1);




     }

}



