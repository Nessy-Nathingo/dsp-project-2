import ballerina/kafka;
import ballerina/log;
import ballerina/kubernetes;
import ballerina/http;
import ballerinax/docker;



kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9090",

    clientId: "basic-producer",
    acks: "all",
    retryCount: 3
};

kafka:Producer kafkaProducer = checkpanic new (producerConfiguration);


endpoint http:Listener registerEP {
    port:8080
};

@http:ServiceConfig{
    basepath: "/registerVoter"
}
@docker:Config {
	name: "producer",
	tag: "v1.0"
}

@kubernetes:Service {
    serviceType:"NodePort";
    name: "votingsystem";
}

endpoint graphql:Listener graphqlListener{
    port:9091;
}

@kubernetes:Deployment {
    image:"producer";
    name:"votingsystem";
}
@http:ServiceConfig{
    basepath: "/registerVoter"
}

type Voter record {
    string name;
    int id;
};

listener graphql:Listener graphqlListener = new(9091);

service VoterManagement on graphqlListener {

@http:ResourceConfig {
        path : "/registration",
        methods : ["POST"]
    }
     resource function registration(http:Caller caller, http:Request request) {

    Voter voter1 = {
        name: "Thomas",
        id: 20201
      }
    var sendRes = kafkaProducer -> send(voter1.toString(), "registered-voters", partition = 1);


     }

}
service graphql:Service /graphql on new graphql:Listener(4000) {
    resource function get name() returns string {
        return "Thomas";
    }

    resource function get id() returns int {
        return 20201;
    }
}


