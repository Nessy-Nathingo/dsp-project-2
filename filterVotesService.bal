import ballerina/io;
import ballerina/kafka;
import ballerina/log;
import ballerina/docker;

string topicPreprocessedVotes = "pre-processed-Votes";
string topicRejectedVotes = "rejected-Votes";
string topicAcceptedVotes = "accepted-Votes";

kafka:ConsumerConfig consumerConfigs = {
    bootstrapServers: "localhost:9092",
    groupId: "vote-filter-consumer",
    topics: [topicPreprocessedVotes]
};

listener kafka:Consumer cons = new(consumerConfigs);

@docker:Config {
	name: "consumer",
	tag: "v1.0"
}

type Vote record {
    string voterName;
    Candidate candidate;
};

type ProcessedVote record {
    boolean isFraud;
    Vote vote;
};

service filterVoteService on cons {
    resource function onMessage(kafka:SimpleConsumer simpleConsumer, kafka:ConsumerRecord[] records) {
        foreach var entry in records {
            byte[] serializedMsg = entry.value;
            string msg = encoding:byteArrayToString(serializedMsg);

            io:StringReader receivedString = new(msg);
            var receivedVoteJson = receivedString.readJson();
            if (receivedVoteJson is error) {
                io:print("Error Parsing Json: ");
                io:println(<string> receivedVoteJson.detail().message);
            } else {
                string topicToPublish = "";
                var receivedVote = ProcessedVote.convert(receivedVoteJson);
                if (receivedVote is error) {
                    io:print("Could not convert recevied vote json to Record type - Receivedvote: ");
                    io:println(<string> receivedVote.detail().message);
                } else {
                    if (receivedVote.isFraud) {
                        topicToPublish = topicRejectedVotes;
                        io:println("Fraudelent vote found");
                    } else if (!receivedVote.isFraud) {
                        topicToPublish = topicAcceptedVotes;
                        io:println("Vote is fine. Sending to publish");
                    }

                    byte[] voteToSend = receivedVoteJson.vote.toString().toByteArray("UTF-8");
                    io:println("Try to send vote to the topic: " + topicToPublish);
                    var sendResult = filteredvotesProducer->send(voteToSend, topicToPublish);
                    io:println("Vote sucessfully sent.");
                    if (sendResult is error) {
                        io:print("Sending the vote failed: ");
                        io:println(<string> sendResult.detail().message);
                    } else {
                        io:println("Vote Published to: \"" + topicToPublish + "\"");
                    }
                }
            }
            io:println("Received Message:\n" + msg + "\n\n");
        }
    }
}