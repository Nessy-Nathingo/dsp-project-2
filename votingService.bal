import ballerina/http;
import ballerina/log;
import ballerina/io;

string topicRegisteredCandidates = "registered-candidates"
string topicRegisteredVoters = "registered-voters"


kafka:ConsumerConfig consumerConfig = {
    bootstrapServers: "localhost:9092, localhost:9093",
    topics: [topicRegisteredCandidates,topicRegisteredVoters], 
    pollingIntervalInMillis: 1000
};

@docker:Expose{}

endpoint graphql:Listener graphqlListener{
    port:9091;
}
@http:ServiceConfig{
    basepath: "/vote"
}
@docker:Config {
	name: "consumer",
	tag: "v1.0"
}

@kubernetes:Service {
    serviceType:"NodePort";
    name: "votingsystem";
}

endpoint graphql:Listener graphqlListener{
    port:9091;
}

@kubernetes:Deployment {
    image:"consumer";
    name:"votingsystem";
}
@http:ServiceConfig{
    basepath: "/vote"
}

listener kafka:SimpleConsumer consumer = new(consumerConfig);

@http:ServiceConfig {
   basePath: "/voting/1.0.0"
}

listener graphql:Listener graphqlListener = new(9091);

service Voting on graphqlListener {

     resource function info(http:Caller caller, http:Request request) {

        int voterID = io:println("Enter your id");
        int candidateID = io:println("Enter id of the candidate you would like to vote for");

        


        




     }

}
